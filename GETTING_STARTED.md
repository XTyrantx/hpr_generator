# Clone the repository

If git is not installed on the operating system, please install it now 
(see the git documentation for [instructions on installing git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)).

To retrieve the code from the repository on anhonesthost.net (a username and 
password are required), run:

`git clone https://repo.anhonesthost.net/rho_n/hpr_generator.git`

To retrieve the code from the repository on gitlab.com, run: 

`git clone https://gitlab.com/roan.horning/hpr_generator.git`

On success, an "hpr_generator" directory will be created in the folder from 
which the clone command was executed containing a local copy of the git repository.

# Install required Perl modules

Installing the Perl modules is the most finicky part of the installation process. 
The needed Perl modules can be found using the operating system's package 
manager or using the modules found in the 
[Comprehensive Perl Archive Network (CPAN)](https://www.cpan.org/).

## Installing modules on Debian based Linux distributions

Run command (tested on Debian 11):

```
apt install libconfig-std-perl \
	libtemplate-perl \
	libtemplate-plugin-dbi-perl \
	libdbd-sqlite3-perl libdate-calc-perl \
	libtie-dbi-perl
```

## Using CPAN to install the modules

A cross platform method to install the needed modules is the Perl CPAN application. 
Make sure both the [make](https://www.gnu.org/software/make/manual/make.html) 
command and the [cpan](https://perldoc.perl.org/CPAN) command are available. 
Install them using the operating system's package manager, or from source.

Run commands:

```
cpan Config::Std
cpan Template
cpan Template::Plugin::DBI
cpan DBD::SQLite
cpan Date::Calc
cpan Tie::DBI
```

# Create the HPR database

The hpr_generator relies on information from a database to generate many of the 
files for the website (for example: index.html, series/index.html, 
hpr_mp3.rss, etc). This data is available from a MySQL dump file found on 
hackerpublicradio.org at URL "https://www.hackerpublicradio.org/hpr.sql".

The first step is to download the hpr.sql file. This can be done using your 
browser, or by running one of the following commands:

`curl https://www.hackerpublicradio.org/hpr.sql --output ./hpr.sql`

or

`wget --directory-prefix=./ https://www.hackerpublicradio.org/hpr.sql`

## Creating an SQLite database file

The SQL of the hpr.sql file must be converted from MySQL specific statements to 
SQLite specific statements. The mysql2sqlite script found in the utils directory 
is used for this conversion. First remove the lines from hpr.sql that 
mysql2sqlite can't handle:

`sed '/^DELIMITER ;;/,/^DELIMITER ;/d' < ./hpr.sql > ./hpr-sqlite.sql`

Next run the mysql2sqlite script piping its output into the sqlite3 
program which creates the hpr.db file:

`./utils/mysql2sqlite ./hpr-sqlite.sql | sqlite3 ./hpr.db`

For convenience, the update-sqlite-db.sh script in the utils directory
automates the above steps (including downloading the hpr.sql file). 
From the root of the local hpr_generator repository run:

`./utils/update-sqlite-db.sh`

# Configure the site-generator

In your favorite text editor, open the site.cfg file found in the root of the 
"hpr_generator" folder. Full details about options for configuring the site.cfg 
file are found in the comments within the file.

## Configuring the database connection

Any database supported by the Perl:DBI and Perl::DBD modules can be used with 
the site-generator program. Currently the hpr_generator project works with 
a MySQL or SQLite database.

Find the [DBI] section of the file. It should look like the following

```
[DBI]
# Configuration settings for SQLite
#database: sqlite
#driver: dbi:SQLite:hpr.db 
#user:        (not used - leave blank)
#password:    (not used - leave blank)
# Configuration settings for MySQL
#database: mysql
#driver: dbi:mysql:database=hpr_hpr:hostname=localhost 
#user: hpr-generator  (Suggested user with read-only privileges)
#password: *********  (Password for user)
```

### SQLite

Remove the comment character from the start of the database and driver 
option lines:

 ```
# Configuration settings for SQLite
database: sqlite
driver: dbi:SQLite:hpr.db 
#user:        (not used - leave blank)
#password:    (not used - leave blank)
```

The hpr.db section of the driver option `dbi:SQLite:hpr.db` is the path 
to the sqlite file. The default assumes the hpr.db file is located in the same
directory as the site-generator. 

### MySQL

Remove the comment character from the start of the database, driver, 
user, and password option lines:

 ```
# Configuration settings for MySQL
database: mysql
driver: dbi:mysql:database=hpr_hpr:hostname=localhost 
user: hpr-generator
password: *********
```

This assumes that the MySQL database service is available at the localhost 
hostname, that the database name (hpr_hpr) is the database created from 
the hpr.sql dump file or manually created by you, that the user (hpr-generator) 
was added by you and has read rights to the hpr_hpr database, and that the 
password (replace ********* with the actual password) matches the password set 
for the hpr-generator database user.

## Configuring the website for viewing locally

For HTML links to work when viewing the files on your local machine using the 
"file://" protocal (i.e. using the "Open..." command in your browser, each HTML 
file must include a \<base\> meta-data tag in the \<head\> section of its 
contents. To configure this in the site.cfg file, find the [root_template] 
section. It should look like the following:

```
[root_template]
content: page.tpl.html
#baseurl: OPTIONAL [i.e. file://<full path to local website directory>]
```
Below the #baseurl comment line add:

```
baseurl: file://</path/to>/hpr_generator/public_html
```

Replace \<path/to\> with the full path to the hpr_generator directory. For 
example: `file:///home/rho_n/development/hpr_generator/public_html`

## Configuring the website media file links

If you do not want to host all the media files (currently, audio files and 
transcription files), you can configure the `media_baseurl` option. This can 
be added to the [root_template] section of the site.cfg file. Suggested 
external site is archive.org. To use this site add:

```
media_baseurl: https://archive.org/download/hpr$eps_id/
```

# Run the site-generator

Run the site generator form the hpr_generator directory:

```
./site-generator --all
```

This will generate all the files for the website. For more examples and to see 
all available options run:

```
./site-generator --help
```
