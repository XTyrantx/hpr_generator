#!/bin/bash - 
#===============================================================================
#
#          FILE: update-hpr-db.sh
# 
#         USAGE: ./update-hpr-db.sh 
# 
#   DESCRIPTION: Download MySQL hpr.sql file and create SQLite3 file
# 
#       OPTIONS: ---
#  REQUIREMENTS: lib_utils.sh
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Roan "Rho`n" Horning (roan.horning@gmail.com)
#  ORGANIZATION: 
#       CREATED: 03/05/2023 07:21:29 PM
#      REVISION:  ---
#       LICENSE: GNU AGPLv3 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.	
#
#===============================================================================

set -o nounset                              # Treat unset variables as an error

BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#
# Load library functions
#
LIB="$BASEDIR/lib_utils.sh"
[ -e $LIB ] || { echo "Unable to load functions.\n$LIB not found."; exit; }
source $LIB

WORKING_DIR=`make_working_dir`

download_hpr_sql $WORKING_DIR

make_hpr_sqlite_db $WORKING_DIR

copy_to_public_dir $WORKING_DIR `pwd`

clean_working_dir $WORKING_DIR
