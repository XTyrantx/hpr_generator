#!/bin/bash - 
#===============================================================================
#
#          FILE: update-hpr.sh
# 
#         USAGE: ./update-hpr.sh 
# 
#   DESCRIPTION: Script to update local statically generated HPR website
# 
#       OPTIONS: ---
#  REQUIREMENTS: lib_utils.sh
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Roan "Rho`n" Horning (roan.horning@gmail.com), 
#  ORGANIZATION: 
#       CREATED: 03/03/2023 09:26:29 PM
#      REVISION:  ---
#       LICENSE: GNU AGPLv3 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.	
#
#===============================================================================

set -o nounset                              # Treat unset variables as an error

BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#
# Load library functions
#
LIB="$BASEDIR/lib_utils.sh"
[ -e $LIB ] || { echo "Unable to load functions.\n$LIB not found."; exit; }
source $LIB

WORKING_DIR=`make_working_dir`

download_hpr_sql $WORKING_DIR

make_hpr_sqlite_db $WORKING_DIR

copy_to_public_dir $WORKING_DIR `pwd`

mv hpr.sql public_html/

echo "Update static HTML files"

# Clean up previously generated files
rm -rf public_html/*.html public_html/correspondents public_html/eps public_html/series

git restore public_html/will-my-show-be-of-interest-to-hackers.html

# stash changes to configuration file to preserve DB settings
git stash

git pull

git stash pop

./site-generator --quiet --all 

clean_working_dir $WORKING_DIR
