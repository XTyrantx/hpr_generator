-- licenses definition

CREATE TABLE licenses (
	id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	short_name VARCHAR(11) NOT NULL,
	long_name VARCHAR(40) NOT NULL,
	url VARCHAR(80) NOT NULL
);
