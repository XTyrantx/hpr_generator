# hpr_generator
Static web page generator for the Hacker Public Radio website.

## Installation
* Clone or download this repository
* With SQLite
	* Create the sqlite3 database from the hpr.sql MySQL dump file available on 
	  hackerpublicradio.org. The default name for the database file is "hpr.db" 
	  and should be located in the root of the project directory. The name and 
	  location can be set in the site.cfg file.
	* An "update-hpr.sh" helper script is available in the utils directory. This 
	  script will download the hpr.sql file, convert it to the SQLite hpr.db file, 
	  and regenerate the website using the site-generator. 
		1. `cd` into the root of the project directory
		2.  Run `./utils/update-hpr.sh`
	* SQLite v3.8.3 or greater is recommended. CTE WITH clauses are used in some template queries. Must convert WITH 
	  clauses to sub-queries when using earlier versions of SQLite.
* With MySQL
	* Create database hpr_hpr in the MySQL server from HPR dump file.
		- ``sudo mysql --host=localhost < hpr.sql``
	* Create a user that will be used by the site-generator.
		- Suggested username: hpr-generator
		- ``CREATE USER 'hpr-generator'@'localhost' IDENTIFIED BY '<password>';``
	* Limit the user's privileges to EXECUTE and SELECT
		- ``GRANT SELECT ON hpr_hpr.* TO 'hpr-generator'@'localhost';``
		- ``GRANT EXECUTE ON `hpr_hpr`.* TO 'hpr-generator'@'localhost';``
* Install the needed Perl modules using preferred method (distribution packages, CPAN, etc.)
    * GetOpt
    * Pod::Usage
    * Config::Std
    * Template
    * Template::Plugin::File
    * Template::Plugin::DBI
    * DBI
    * Tie::DBI
    * DBD::SQLite or DBD:mysql
    * Date::Calc
* See the Getting Started tutorial (GETTING_STARTED.md) for more details on 
  installing the HPR generator.
## Usage
Generate two specific pages:
`site-generator index about`		

Generate the whole site:
`site-generator --all`	

Generate pages based on the same template:
`site-generator correspondent=1,3,5..10`

## Support
Please [submit an Issue](https://repo.anhonesthost.net/rho_n/hpr_generator/issues),
and add the label "**Help Request**" for help running or installing the site-generator.

For discussing HPR site generation in general, please [submit an Issue](https://repo.anhonesthost.net/rho_n/hpr_generator/issues) and add the label "**General Discussion**".

## Contributing
Happy to take any contributions or suggestions.

To contribute code or documentation, please create a fork of the project and [submit a pull request](https://repo.anhonesthost.net/rho_n/hpr_generator/pulls) or send a patch. If an issue exists that is related to your patch, please assign the issue to yourself, or if it is already assigned to someone else, please coordinate with them to minimize duplicated efforts.

If you have found an error in the code or the generated HTML, please [submit an Issue](https://repo.anhonesthost.net/rho_n/hpr_generator/issues),
and add the label "**Bug Found**".

To make a suggestion, please [submit an Issue](https://repo.anhonesthost.net/rho_n/hpr_generator/issues),
and add the label "**Feature Request**".

## Authors and acknowledgment
* Roan "Rho`n" Horning 
* gordons
* Ken Fallon
* norrist